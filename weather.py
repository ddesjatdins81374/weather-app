#! /usr/bin/env python3
import requests
import argparse

parser = argparse.ArgumentParser(description='Use Time in Unix format to get weather at that time.')
parser.add_argument('--time', help='time please in seconds')

args = parser.parse_args()

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    """
    URL = "http://ipvigilante.com/"
    r = requests.get(url = URL)
    data = r.json()
    latitude = data["data"]["latitude"]
    longitude = data["data"]["longitude"] 
    city_name = data["data"]["city_name"]

    return (longitude, latitude, city_name)

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
    """
    
    URL = 'https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY + '/' + latitude + ',' + longitude

    if args.time:
        URL = URL + ',' + str(args.time)    
 
    r = requests.get(url = URL)
    data = r.json()
    
    visability = data["hourly"]["data"][0]["summary"]
    temp = data["currently"]["temperature"] 
    temp_high = data["daily"]["data"][0]["temperatureHigh"]
    temp_low = data["daily"]["data"][0]["temperatureLow"]    
    humidity = data["hourly"]["data"][0]["humidity"] 
    feels_like = data["currently"]["apparentTemperature"]

    return (visability,temp,temp_high,temp_low,humidity,feels_like)

def print_forecast(temp, visability):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("The weather forcast in " + str(city_name) + "\n")
    print(str(visability) + "\n")
    print("Temperature: " + str(temp))
    print("High: " + str(temp_high))
    print("Low: " + str(temp_low))
    print("Humidity: " + str(humidity))
    print("Feels like: " + str(feels_like))

if __name__ == "__main__":
    longitude, latitude, city_name = get_location()
    visability,temp,temp_high,temp_low,humidity,feels_like = get_temperature(longitude, latitude)
    print_forecast(temp, visability)

